package comebackisreal.com.codeexample1


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_fragment_one.view.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private const val EXTRA_NAMA ="nama"
private const val EXTRA_UMUR ="umur"
private const val EXTRA_EMAIL ="email"

class FragmentOne : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_fragment_one, container, false)
        var btn = view.btnKirim
        btn.setOnClickListener{kirimPesan()}
        return view
    }

    private fun kirimPesan(){
        val secondFragment = FragmentTwo()
        val secondFragmentManager = fragmentManager
        val secondTransaction = secondFragmentManager!!.beginTransaction()

        val myBundle = Bundle()
        myBundle.putString(EXTRA_NAMA,view!!.txtNama.text.toString())
        myBundle.putString(EXTRA_UMUR,view!!.txtUmur.text.toString())
        myBundle.putString(EXTRA_EMAIL,view!!.txtEmail.text.toString())

        secondFragment.arguments = myBundle

        secondTransaction.replace(R.id.containerTwo, secondFragment, secondFragment.javaClass.simpleName)
        secondTransaction.addToBackStack(null)
        secondTransaction.commit()

        view!!.txtNama.text = null
        view!!.txtUmur.text = null
        view!!.txtEmail.text = null

    }
}
