package comebackisreal.com.codeexample1


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_fragment_one.view.*
import kotlinx.android.synthetic.main.fragment_fragment_two.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private const val EXTRA_NAMA ="nama"
private const val EXTRA_UMUR ="umur"
private const val EXTRA_EMAIL ="email"

class FragmentTwo : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_fragment_two, container, false)
        if(arguments?.isEmpty==false){
            view.lblInputNama.text = arguments!![EXTRA_NAMA].toString()
            view.lblInputUmur.text = arguments!![EXTRA_UMUR].toString()
            view.lblInputEmail.text = arguments!![EXTRA_EMAIL].toString()
        }
        return view
    }


}
