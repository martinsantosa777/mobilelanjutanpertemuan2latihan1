package comebackisreal.com.codeexample1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentManager = supportFragmentManager
        var fragmentTransction = fragmentManager.beginTransaction()

        val myFragmentOne = FragmentOne()
        val myFragmentTwo = FragmentTwo()

        fragmentTransction.add(R.id.containerOne, myFragmentOne, myFragmentOne.javaClass.simpleName)
        fragmentTransction.add(R.id.containerTwo, myFragmentTwo, myFragmentTwo.javaClass.simpleName)
        fragmentTransction.commit()
    }
}
